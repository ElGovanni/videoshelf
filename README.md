# videoshelf
PWA for movies list

[![pipeline status](https://gitlab.com/StefanTouhami/videoshelf/badges/master/pipeline.svg)](https://ElGovanni.gitlab.io/videoshelf)

<h2>[Demo](https://StefanTouhami.gitlab.io/videoshelf)</h2>
<img alt="Preview" src="https://i.imgur.com/ZFshj0K.png" />

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```
